# Mock VCF

## What is this for?
This mock VCF is meant to test MORL data pipelines.
You can compare the output of your pipeline with the
data in this VCF to verify that it's working correctly.

One example test would be to verify that a variant's
pathogenicity matches the pathogenicity in the output
of the pipeline.

## Importing the VCF into Galaxy

In Galaxy, you can upload the VCF directly from this repository. To do that you will need to:

1. Go to Galaxy, and click **Upload File** (under the *Get Data* link on the left-hand side).
1. In the textbox titled **URL/Text**, paste the following URL: `https://bitbucket.org/sephraim/mockvcf/raw/master/mock.vcf`
1. In the **Genome** dropdown menu, select **Human Feb. 2009 (GRCh37/hg19)(hg19)**.

## Updating the VCF
You can add your own variants and/or INFO tags to this VCF, and then validate it to ensure that it is formatted correctly using the `validate.sh` script.

**NOTE:** The `validate.sh` script requires that you have `vcf-validator`, `tabix`, and `bgzip` in your `$PATH`. It will not work otherwise.

### Example usage
    ./validate.sh mock.vcf
    
Some warnings such as "*The header tag 'contig' not present for CHROM=10...*" are bound to occur and can be ignored.

Once the VCF is properly validated, you can commit the changes and push them to this repository.