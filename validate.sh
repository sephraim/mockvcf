#!/usr/bin/env bash

##
# Validate that the VCF file is formatted correctly
#
# Example usage:
#   # Use default file (i.e. mock.vcf)
#   ./validate.sh
#   # Specify your own file
#   ./validate.sh my.vcf
##

if [ -z "$1" ]; then
  # Set default file
  file='mock.vcf'
  echo "No file specified. Testing default file [$file]..."
else
  # Set user-specified file
  file=$1
fi

echo -n "Compressing... "
bgzip -c $file > $file.gz
echo "Done!"
echo -n "Indexing... "
tabix -p vcf $file.gz
echo "Done!"
echo "Validating..."
vcf-validator $file.gz
echo "Done!"

if [ "$file" == "mock.vcf" ]; then
  echo "Testing tabix query..."
  query="tabix $file.gz chr7:107323939-107323939"
  echo "QUERY: $query"
  echo "RESULT:"
  $query
fi
