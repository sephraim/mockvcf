#!/bin/env ruby

F_IN = 'DVD_v8.2016-11-04.vcf.gz'
F_HEADER = 'header.vcf'

# Print header
puts File.readlines(F_HEADER)

## Prediction tests (7 total)
## - Test to see if predictions are working okay
#max = 6
#(0..max).each do |i|
#  line = `bcftools query -r #{i+1} -i 'NUM_PATH_PREDS="#{i}" && TOTAL_NUM_PREDS="#{max}"' \
#          -f '%CHROM\t%POS\t.\t%REF\t%ALT\t.\t.\tASSERT_GENE=%GENE;ASSERT_NUM_PATH_PREDS=%NUM_PATH_PREDS;ASSERT_TOTAL_NUM_PREDS=%TOTAL_NUM_PREDS;ASSERT_FINAL_PRED=%FINAL_PRED;TEST_FOR=Final_prediction_based_on_pathogenicity_score\n' \
#          #{F_IN} | head -1`
#  puts line unless line.empty?
#end
#
## Manually curated overrides (6 total)
#CURATED_CLNSIGS = [
#  {:curated_path => 'Benign',                 :final_path => 'Benign'},
#  {:curated_path => 'Benign*',                :final_path => 'Benign*'},
#  {:curated_path => 'Likely%20benign',        :final_path => 'Likely%20benign'},
#  {:curated_path => 'Unknown%20significance', :final_path => 'Unknown%20significance'},
#  {:curated_path => 'Likely%20pathogenic',    :final_path => 'Likely%20pathogenic'},
#  {:curated_path => 'Pathogenic',             :final_path => 'Pathogenic'},
#]
#CURATED_CLNSIGS.each do |clnsig|
#  line = `bcftools query -i 'CURATED_PATHOGENICITY = "#{clnsig[:curated_path]}" && FINAL_PATHOGENICITY = "#{clnsig[:final_path]}" && CURATED_PMID != "."' \
#            -f '%CHROM\t%POS\t.\t%REF\t%ALT\t.\t.\tASSERT_GENE=%GENE;ASSERT_CURATED_PATHOGENICITY=%CURATED_PATHOGENICITY;ASSERT_CURATED_PMID=%CURATED_PMID;ASSERT_FINAL_PATHOGENICITY=%FINAL_PATHOGENICITY;TEST_FOR=Manual_curation_(#{clnsig[:curated_path].gsub('%20', '_')})\n' \
#            #{F_IN} | head -1`
#
#  # If no results, try again without PMID restriction
#  if line.empty?
#    line = `bcftools query -i 'CURATED_PATHOGENICITY = "#{clnsig[:curated_path]}" && FINAL_PATHOGENICITY = "#{clnsig[:final_path]}"' \
#              -f '%CHROM\t%POS\t.\t%REF\t%ALT\t.\t.\tASSERT_GENE=%GENE;ASSERT_CURATED_PATHOGENICITY=%CURATED_PATHOGENICITY;ASSERT_CURATED_PMID=%CURATED_PMID;ASSERT_FINAL_PATHOGENICITY=%FINAL_PATHOGENICITY;TEST_FOR=Manual_curation_(#{clnsig[:curated_path].gsub('%20', '_')})\n' \
#              #{F_IN} | head -1`
#  end
#  puts line unless line.empty?
#end
#
## Only found in HGMD (should be 14 total, but only 6 are present)
## - Ensure that original pathogenicity + confidence creates the right final pathogenicity
#HGMD_CLNSIGS = [
#  {:hgmd_path => 'DM',  :conf => 'Low',  :final_path => 'Likely%20pathogenic'},
#  {:hgmd_path => 'DM',  :conf => 'High', :final_path => 'Pathogenic'},
#  {:hgmd_path => 'DM?', :conf => 'Low',  :final_path => 'Unknown%20significance'},
#  {:hgmd_path => 'DM?', :conf => 'High', :final_path => 'Unknown%20significance'},
#  {:hgmd_path => 'CNV', :conf => 'High', :final_path => 'Benign'},
#  {:hgmd_path => 'CNV', :conf => 'Low',  :final_path => 'Likely%20benign'},
#  {:hgmd_path => 'FTV', :conf => 'High', :final_path => 'Benign'},
#  {:hgmd_path => 'FTV', :conf => 'Low',  :final_path => 'Likely%20benign'},
#  {:hgmd_path => 'FP',  :conf => 'High', :final_path => 'Benign'},
#  {:hgmd_path => 'FP',  :conf => 'Low',  :final_path => 'Likely%20benign'},
#  {:hgmd_path => 'DFP', :conf => 'High', :final_path => 'Benign'},
#  {:hgmd_path => 'DFP', :conf => 'Low',  :final_path => 'Likely%20benign'},
#  {:hgmd_path => 'DP',  :conf => 'High', :final_path => 'Benign'},
#  {:hgmd_path => 'DP',  :conf => 'Low',  :final_path => 'Likely%20benign'},
#]
#HGMD_CLNSIGS.each do |clnsig|
#  line = `bcftools query -i 'HGMD_VARIANTTYPE = "#{clnsig[:hgmd_path]}" && HGMD_CONFIDENCE = "#{clnsig[:conf]}" && FINAL_PATHOGENICITY = "#{clnsig[:final_path]}"' \
#            -f '%CHROM\t%POS\t.\t%REF\t%ALT\t.\t.\tASSERT_GENE=%GENE;ASSERT_HGMD_VARIANTTYPE=%HGMD_VARIANTTYPE;ASSERT_HGMD_CONFIDENCE=%HGMD_CONFIDENCE;ASSERT_FINAL_PATHOGENICITY=%FINAL_PATHOGENICITY;TEST_FOR=HGMD_exclusive_pathogenicity_(%FINAL_PATHOGENICITY)\n' \
#            #{F_IN} | grep -m1 -v 'CLINVAR_CLNSIG'`
#  puts line unless line.empty?
#end
#
## Only found in ClinVar (6 total)
#CLINVAR_CLNSIGS = [
#  # Single submission
#  {:clinvar_path => 'Pathogenic',               :final_path => 'Pathogenic'},
#  {:clinvar_path => 'Likely%20pathogenic',      :final_path => 'Likely%20pathogenic'},
#  {:clinvar_path => 'Uncertain%20significance', :final_path => 'Unknown%20significance'},
#  {:clinvar_path => 'Likely%20benign',          :final_path => 'Likely%20benign'},
#  {:clinvar_path => 'Benign',                   :final_path => 'Benign'},
#  # Multiple submissions
#  {:clinvar_path => 'Benign%3BLikely%20benign%3BPathogenic', :final_path => 'Pathogenic'},
#]
#CLINVAR_CLNSIGS.each_with_index do |clnsig, i|
#  line = `bcftools query -r #{22-i} -i 'CLINVAR_CLNSIG = "#{clnsig[:clinvar_path]}" && FINAL_PATHOGENICITY = "#{clnsig[:final_path]}"' \
#            -f '%CHROM\t%POS\t.\t%REF\t%ALT\t.\t.\tASSERT_GENE=%GENE;ASSERT_CLINVAR_CLNSIG=%CLINVAR_CLNSIG;ASSERT_FINAL_PATHOGENICITY=%FINAL_PATHOGENICITY;TEST_FOR=ClinVar_exclusive_pathogenicity_(#{clnsig[:final_path].gsub('%20', '_').gsub('%3B', ',')})\n' \
#            #{F_IN} | grep -m1 -v 'HGMD_VARIANTTYPE'`
#
#  # If no results, try again without chromosome restriction
#  if line.empty?
#    line = `bcftools query -i 'CLINVAR_CLNSIG = "#{clnsig[:clinvar_path]}" && FINAL_PATHOGENICITY = "#{clnsig[:final_path]}"' \
#              -f '%CHROM\t%POS\t.\t%REF\t%ALT\t.\t.\tASSERT_GENE=%GENE;ASSERT_CLINVAR_CLNSIG=%CLINVAR_CLNSIG;ASSERT_FINAL_PATHOGENICITY=%FINAL_PATHOGENICITY;TEST_FOR=ClinVar_exclusive_pathogenicity_(#{clnsig[:final_path].gsub('%20', '_').gsub('%3B', ',')})\n' \
#              #{F_IN} | grep -m1 -v 'HGMD_VARIANTTYPE'`
#  end
#  puts line unless line.empty?
#end
#
#DB_CLNSIGS = [
#  # ClinVar (single submission) and HGMD agree
#  {:clinvar_path => 'Pathogenic', :hgmd_path => 'DM',  :hgmd_conf => 'High', :final_path => 'Pathogenic', :test_for => 'ClinVar_and_HGMD_agree_(Pathogenic)'},
#  {:clinvar_path => 'Likely%20pathogenic', :hgmd_path => 'DM',  :hgmd_conf => 'Low',  :final_path => 'Likely%20pathogenic', :test_for => 'ClinVar_(single_submission)_and_HGMD_agree_(Likely_pathogenic)'},
#  {:clinvar_path => 'Uncertain%20significance', :hgmd_path => 'DM?', :hgmd_conf => 'High', :final_path => 'Unknown%20significance', :test_for => 'ClinVar_(single_submission)_and_HGMD_agree_(Unknown_significance)'},
#  {:clinvar_path => 'Uncertain%20significance', :hgmd_path => 'DM?', :hgmd_conf => 'Low',  :final_path => 'Unknown%20significance', :test_for => 'ClinVar_(single_submission)_and_HGMD_agree_(Unknown_significance)'},
#  {:clinvar_path => 'Likely%20benign', :hgmd_path => 'CNV', :hgmd_conf => 'Low',  :final_path => 'Likely%20benign', :test_for => 'ClinVar_(single_submission)_and_HGMD_agree_(Likely_benign)'},
#  {:clinvar_path => 'Benign', :hgmd_path => 'CNV', :hgmd_conf => 'High', :final_path => 'Benign', :test_for => 'ClinVar_(single_submission)_and_HGMD_agree_(Benign)'},
#  {:clinvar_path => 'Likely%20benign', :hgmd_path => 'FTV', :hgmd_conf => 'Low',  :final_path => 'Likely%20benign', :test_for => 'ClinVar_(single_submission)_and_HGMD_agree_(Likely_benign)'},
#  {:clinvar_path => 'Benign', :hgmd_path => 'FTV', :hgmd_conf => 'High', :final_path => 'Benign', :test_for => 'ClinVar_(single_submission)_and_HGMD_agree_(Benign)'},
#  {:clinvar_path => 'Likely%20benign', :hgmd_path => 'FP',  :hgmd_conf => 'Low',  :final_path => 'Likely%20benign', :test_for => 'ClinVar_(single_submission)_and_HGMD_agree_(Likely_benign)'},
#  {:clinvar_path => 'Benign', :hgmd_path => 'FP',  :hgmd_conf => 'High', :final_path => 'Benign', :test_for => 'ClinVar_(single_submission)_and_HGMD_agree_(Benign)'},
#  {:clinvar_path => 'Likely%20benign', :hgmd_path => 'DFP', :hgmd_conf => 'Low',  :final_path => 'Likely%20benign', :test_for => 'ClinVar_(single_submission)_and_HGMD_agree_(Likely_benign)'},
#  {:clinvar_path => 'Benign', :hgmd_path => 'DFP', :hgmd_conf => 'High', :final_path => 'Benign', :test_for => 'ClinVar_(single_submission)_and_HGMD_agree_(Benign)'},
#  {:clinvar_path => 'Likely%20benign', :hgmd_path => 'DP',  :hgmd_conf => 'Low',  :final_path => 'Likely%20benign', :test_for => 'ClinVar_(single_submission)_and_HGMD_agree_(Likely_benign)'},
#  {:clinvar_path => 'Benign', :hgmd_path => 'DP',  :hgmd_conf => 'High', :final_path => 'Benign', :test_for => 'ClinVar_(single_submission)_and_HGMD_agree_(Benign)'},
#
#  # ClinVar (multiple submissions) and HGMD agree
#  {:clinvar_path => 'Pathogenic%3BUncertain%20significance', :hgmd_path => 'DM',  :hgmd_conf => 'High', :final_path => 'Pathogenic', :test_for => 'ClinVar_(multiple_submissions)_and_HGMD_agree_(Pathogenic)'},
#  {:clinvar_path => 'Benign%3BUncertain%20significance', :hgmd_path => 'CNV\|DFP\|DP\|FP\|FTV',  :hgmd_conf => 'High', :final_path => 'Benign', :test_for => 'ClinVar_(multiple_submissions)_and_HGMD_agree_(Benign)'},
#
#  # ClinVar (single submission) and HGMD mostly agree
#  {:clinvar_path => 'Pathogenic', :hgmd_path => 'DM',  :hgmd_conf => 'Low', :final_path => 'Likely%20pathogenic', :test_for => 'ClinVar_(single_submission)_and_HGMD_mostly_agree_(Pathogenic/DM-Low)'},
#  {:clinvar_path => 'Likely%20pathogenic', :hgmd_path => 'DM',  :hgmd_conf => 'High', :final_path => 'Likely%20pathogenic', :test_for => 'ClinVar_(single_submission)_and_HGMD_mostly_agree_(Likely_pathogenic/DM-High)'},
#  {:clinvar_path => 'Likely%20benign', :hgmd_path => 'CNV\|DFP\|DP\|FP\|FTV',  :hgmd_conf => 'High', :final_path => 'Likely%20pathogenic', :test_for => 'ClinVar_(single_submission)_and_HGMD_mostly_agree_(Likely_benign/CNV|DFP|DP|FP|FTV-High)'},
#  {:clinvar_path => 'Benign', :hgmd_path => 'CNV\|DFP\|DP\|FP\|FTV',  :hgmd_conf => 'Low', :final_path => 'Likely%20pathogenic', :test_for => 'ClinVar_(single_submission)_and_HGMD_mostly_agree_(Benign/CNV|DFP|DP|FP|FTV-Low)'},
#
#  # ClinVar (multiple submissions) and HGMD mostly agree
#  # - None exist in DVD v8
#
#  # ClinVar (single submission) and HGMD disagree
#  {:clinvar_path => 'Benign',          :hgmd_path => 'DM', :hgmd_conf => 'High', :final_path => 'Unknown%20significance', :test_for => 'ClinVar_(single_submission)_and_HGMD_disagree_(Benign/DM-High)'},
#  {:clinvar_path => 'Benign',          :hgmd_path => 'DM', :hgmd_conf => 'Low',  :final_path => 'Unknown%20significance', :test_for => 'ClinVar_(single_submission)_and_HGMD_disagree_(Benign/DM-Low)'},
#  {:clinvar_path => 'Likely%20benign', :hgmd_path => 'DM', :hgmd_conf => 'High', :final_path => 'Unknown%20significance', :test_for => 'ClinVar_(single_submission)_and_HGMD_disagree_(Likely_benign/DM-High)'},
#  {:clinvar_path => 'Likely%20benign', :hgmd_path => 'DM', :hgmd_conf => 'Low',  :final_path => 'Unknown%20significance', :test_for => 'ClinVar_(single_submission)_and_HGMD_disagree_(Likely_benign/DM-Low)'},
#
#  {:clinvar_path => 'Likely%20pathogenic', :hgmd_path => 'CNV\|DFP\|DP\|FP\|FTV',  :hgmd_conf => 'High', :final_path => 'Unknown%20significance', :test_for => 'ClinVar_(single_submission)_and_HGMD_disagree_(Likely_pathogenic/CNV|DFP|DP|FP|FTV-High)'},
#  {:clinvar_path => 'Likely%20pathogenic', :hgmd_path => 'CNV\|DFP\|DP\|FP\|FTV',  :hgmd_conf => 'Low',  :final_path => 'Unknown%20significance', :test_for => 'ClinVar_(single_submission)_and_HGMD_disagree_(Likely_pathogenic/CNV|DFP|DP|FP|FTV-Low)'},
#  {:clinvar_path => 'Pathogenic',          :hgmd_path => 'CNV\|DFP\|DP\|FP\|FTV',  :hgmd_conf => 'High', :final_path => 'Unknown%20significance', :test_for => 'ClinVar_(single_submission)_and_HGMD_disagree_(Pathogenic/CNV|DFP|DP|FP|FTV-High)'},
#  {:clinvar_path => 'Pathogenic',          :hgmd_path => 'CNV\|DFP\|DP\|FP\|FTV',  :hgmd_conf => 'Low',  :final_path => 'Unknown%20significance', :test_for => 'ClinVar_(single_submission)_and_HGMD_disagree_(Pathogenic/CNV|DFP|DP|FP|FTV-Low)'},
#
#  # ClinVar (multiple submissions) and HGMD disagree
#  {:clinvar_path => 'Benign%3BLikely%20benign%3BUncertain%20significance', :hgmd_path => 'DM', :hgmd_conf => 'Low',  :final_path => 'Unknown%20significance', :test_for => 'ClinVar_(multiple_submissions)_and_HGMD_disagree_(B|LB|VUS/DM-Low)'},
#  {:clinvar_path => 'Likely%20benign%3BUncertain%20significance', :hgmd_path => 'DM', :hgmd_conf => 'High',  :final_path => 'Unknown%20significance', :test_for => 'ClinVar_(multiple_submissions)_and_HGMD_disagree_(LB|VUS/DM-High)'},
#]
#DB_CLNSIGS.each do |clnsig|
#  line = `bcftools query -i 'CLINVAR_CLNSIG = "#{clnsig[:clinvar_path]}" && HGMD_VARIANTTYPE ~ "#{clnsig[:hgmd_path]}" && HGMD_CONFIDENCE = "#{clnsig[:hgmd_conf]}" && FINAL_PATHOGENICITY_SOURCE = "ClinVar/HGMD"' \
#            -f '%CHROM\t%POS\t.\t%REF\t%ALT\t.\t.\tASSERT_GENE=%GENE;ASSERT_CLINVAR_CLNSIG=%CLINVAR_CLNSIG;ASSERT_HGMD_VARIANTTYPE=%HGMD_VARIANTTYPE;ASSERT_HGMD_CONFIDENCE=%HGMD_CONFIDENCE;ASSERT_FINAL_PATHOGENICITY_SOURCE=ClinVar/HGMD;ASSERT_FINAL_PATHOGENICITY=%FINAL_PATHOGENICITY;TEST_FOR=#{clnsig[:test_for]})\n' \
#            #{F_IN} | head -1`
#  puts line unless line.empty?
#end

# Benign because of high MAF
MAFS = [
  '1KG_AFR_AF',
  '1KG_AMR_AF',
  '1KG_EAS_AF',
  '1KG_EUR_AF',
  '1KG_SAS_AF',
  'EVS_EA_AF',
  'EVS_AA_AF',
  'EXAC_AFR_AF',
  'EXAC_AMR_AF',
  'EXAC_EAS_AF',
  'EXAC_FIN_AF',
  'EXAC_NFE_AF',
  'EXAC_OTH_AF',
  'EXAC_SAS_AF',
  'OTO_AJ_AF',
  'OTO_CO_AF',
  'OTO_US_AF',
  'OTO_JP_AF',
  'OTO_ES_AF',
  'OTO_TR_AF',
]
# MAF > 0.005
MAFS.each_with_index do |maf|
  exclude_str = (MAFS - [maf]).map {|m| "(#{m} < 0.005 || #{m} == \".\")"}.join(' && ')
  line = `bcftools query -i '#{maf} > 0.005 && FINAL_PATHOGENICITY_SOURCE = "MAF" && #{exclude_str}' \
            -f '%CHROM\t%POS\t.\t%REF\t%ALT\t.\t.\tASSERT_GENE=%GENE;ASSERT_#{maf}=%#{maf};ASSERT_FINAL_PATHOGENICITY=%FINAL_PATHOGENICITY;ASSERT_FINAL_PATHOGENICITY_SOURCE=MAF;TEST_FOR=Benign_because_MAF>0.005_in_#{maf}_only\n' \
            #{F_IN} | head -1`

  # If no results, try again without exclusions
  if line.empty?
    line = `bcftools query -i '#{maf} > 0.005 && FINAL_PATHOGENICITY_SOURCE = "MAF"' \
              -f '%CHROM\t%POS\t.\t%REF\t%ALT\t.\t.\tASSERT_GENE=%GENE;ASSERT_#{maf}=%#{maf};ASSERT_FINAL_PATHOGENICITY=%FINAL_PATHOGENICITY;ASSERT_FINAL_PATHOGENICITY_SOURCE=MAF;TEST_FOR=Benign_because_MAF>0.005_in_#{maf}_(as_well_as_others)\n' \
              #{F_IN} | head -1`
  end
  puts line unless line.empty?
end
# MAF == 0.005
MAFS.each_with_index do |maf|
  exclude_str = (MAFS - [maf]).map {|m| "(#{m} < 0.005 || #{m} == \".\")"}.join(' && ')
  line = `bcftools query -i '#{maf} == 0.005 && FINAL_PATHOGENICITY_SOURCE = "MAF" && #{exclude_str}' \
            -f '%CHROM\t%POS\t.\t%REF\t%ALT\t.\t.\tASSERT_GENE=%GENE;ASSERT_#{maf}=%#{maf};ASSERT_FINAL_PATHOGENICITY=%FINAL_PATHOGENICITY;ASSERT_FINAL_PATHOGENICITY_SOURCE=MAF;TEST_FOR=Benign_because_MAF_equals_0.005_in_#{maf}_only\n' \
            #{F_IN} | head -1`

  # If no results, try again without exclusions
  if line.empty?
    line = `bcftools query -i '#{maf} == 0.005 && FINAL_PATHOGENICITY_SOURCE = "MAF"' \
              -f '%CHROM\t%POS\t.\t%REF\t%ALT\t.\t.\tASSERT_GENE=%GENE;ASSERT_#{maf}=%#{maf};ASSERT_FINAL_PATHOGENICITY=%FINAL_PATHOGENICITY;ASSERT_FINAL_PATHOGENICITY_SOURCE=MAF;TEST_FOR=Benign_because_MAF_equals_0.005_in_#{maf}_(as_well_as_others)\n' \
              #{F_IN} | head -1`
  end
  puts line unless line.empty?
end
